﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Rope : MonoBehaviour {

    #region FIELDS
    [Header("Generation")]
    public float RopeLen = 10;
    public int SegmentsCount = 10;
    public float ColliderRadius = 0.03f;

    [Header("Segments settings")]
    public Rigidbody TipObject;
    public Vector3 TipObjectPivot;

    [Header("Vizual")]
    public LineRenderer Line;

    private float perSegDist = 0;
    private List<GameObject> segms = new List<GameObject>();
    private List<Rigidbody> rigids = new List<Rigidbody>();
    private List<ConfigurableJoint> joints = new List<ConfigurableJoint>();
    private List<SphereCollider> cldrs = new List<SphereCollider>();

    private Vector3[] positions;
    #endregion

    void Start () {
        generateRope();
    }

    private void LateUpdate () {
        for (int i = 0; i < segms.Count; i++)
            positions[i] = segms[i].transform.position;
        Line.SetPositions(positions);
    }

    private void generateRope () {
        perSegDist = RopeLen / SegmentsCount;
        segms.Add(gameObject);
        rigids.Add(gameObject.GetComponent<Rigidbody>());
        for (int i = 1; i < SegmentsCount; i++) {
            addSegment(new GameObject());
            GameObject seg = segms[segms.Count - 1];
            Transform trans = seg.transform;
            Vector3 pos = segms[0].transform.position;
            pos.y = segms[0].transform.position.y - i * perSegDist;
            trans.position = pos;

            joints[joints.Count - 1].autoConfigureConnectedAnchor = true;
            joints[joints.Count - 1].anchor = new Vector3(0, perSegDist, 0);
            joints[joints.Count - 1].connectedBody = rigids[i - 1];

            for (int j = i; j < cldrs.Count; j++) {
                Physics.IgnoreCollision(cldrs[i], cldrs[j], true);
            }
        }

        Line.positionCount = segms.Count;
        positions = new Vector3[segms.Count];

        if (TipObject) {
            SphereCollider c = setupSegmCollider(TipObject.gameObject);
            ConfigurableJoint j = setupSegmJoint(TipObject.gameObject);
            Rigidbody r = setupSegmRigid(TipObject.gameObject);

            segms[segms.Count - 1].transform.position = TipObject.transform.TransformPoint(TipObjectPivot);
            j.connectedBody = rigids[rigids.Count - 1];
            j.anchor = TipObjectPivot;
        }
    }
    private void addSegment (GameObject seg) {
        seg.layer = gameObject.layer;
        seg.name = $"segment {segms.Count}";
        Rigidbody rigid = setupSegmRigid(seg);
        ConfigurableJoint joint = setupSegmJoint(seg);
        SphereCollider cldr = setupSegmCollider(seg);

        segms.Add(seg);
        rigids.Add(rigid);
        joints.Add(joint);
        cldrs.Add(cldr);
    }

    private Rigidbody setupSegmRigid (GameObject segm) {
        return Util.getOrCreate<Rigidbody>(segm);
    }
    private ConfigurableJoint setupSegmJoint (GameObject segm) {
        ConfigurableJoint j = Util.getOrCreate<ConfigurableJoint>(segm);
        j.axis = new Vector3(1, 0, 0);
        j.secondaryAxis = new Vector3(0, 1, 0);
        j.xMotion = ConfigurableJointMotion.Locked;
        j.yMotion = ConfigurableJointMotion.Locked;
        j.zMotion = ConfigurableJointMotion.Locked;
        j.projectionMode = JointProjectionMode.PositionAndRotation;
        return j;
    }
    private SphereCollider setupSegmCollider (GameObject segm) {
        SphereCollider c = Util.getOrCreate<SphereCollider>(segm);
        c.radius = ColliderRadius;
        return c;
    }

    private void OnDestroy () {
        for (int i = 0; i < segms.Count; i++) {
            Destroy(segms[i].gameObject);
        }
    }

    private void OnDrawGizmos () {
        for (int i = 1; i < segms.Count; i++) {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(segms[i - 1].transform.position, segms[i].transform.position);
            Gizmos.DrawSphere(segms[i].transform.position, 0.1f);

            Gizmos.color = Color.red;
            Vector3 prevAnchor = rigids[i - 1].transform.TransformPoint(0, -perSegDist, 0);
            Gizmos.DrawCube(prevAnchor, new Vector3(0.2f, 0.05f, 0.05f));
        }

        if (TipObject) {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(TipObject.transform.TransformPoint(TipObjectPivot), 0.05f);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, TipObject.transform.TransformPoint(TipObjectPivot));
        }
    }
}
