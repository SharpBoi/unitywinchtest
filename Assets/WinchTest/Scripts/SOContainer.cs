﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOContainer : MonoBehaviour {

    public static SOContainer main;


    public ClipAreaTable ClipTable;


    void Start () {
        if (!main) main = this;
        else {
            throw new System.Exception("Only one instance allowerd");
        }
    }

}
