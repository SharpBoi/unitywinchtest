﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MouseGrabbable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

    #region FIELDS
    [Header("Lock axis")]
    public bool OverrideX;
    public bool OverrideY;
    public bool OverrideZ;
    public Vector3 OverrideValue;

    [Header("Events")]
    public UnityEvent OnDragStart;
    public UnityEvent OnDragEnd;


    private Rigidbody rigid;
    private bool isDragNow;
    private Vector3 targetDragPnt;
    private float beginDragDst;
    private Vector3 beginDragPntLocal;
    #endregion

    void Start () {
        rigid = GetComponent<Rigidbody>();
    }

    private void FixedUpdate () {
        if (isDragNow) {
            if (rigid) {
                rigid.velocity = (targetDragPnt - transform.position - (beginDragPntLocal)) * 8;
            }
            else {
                this.transform.position = targetDragPnt;
            }
        }
    }

    public void OnBeginDrag (PointerEventData eventData) {
        if (rigid) {
            //rigid.isKinematic = true;
            //rigid.useGravity = false;
        }

        isDragNow = true;
        beginDragDst = eventData.pointerCurrentRaycast.distance;
        beginDragPntLocal = transform.InverseTransformPoint(eventData.pointerCurrentRaycast.worldPosition);
        OnDragStart.Invoke();
    }

    public void OnDrag (PointerEventData eventData) {
        targetDragPnt = eventData.pressEventCamera.transform.position +
            (eventData.pointerCurrentRaycast.worldPosition - eventData.pressEventCamera.transform.position).normalized * beginDragDst;
        targetDragPnt.x = OverrideValue.x * Convert.ToInt32(OverrideX) + targetDragPnt.x * Convert.ToInt32(!OverrideX);
        targetDragPnt.y = OverrideValue.y * Convert.ToInt32(OverrideY) + targetDragPnt.y * Convert.ToInt32(!OverrideY);
        targetDragPnt.z = OverrideValue.z * Convert.ToInt32(OverrideZ) + targetDragPnt.z * Convert.ToInt32(!OverrideZ);
    }

    public void OnEndDrag (PointerEventData eventData) {
        if (rigid) {
            //rigid.isKinematic = false;
            //rigid.useGravity = true;
        }

        isDragNow = false;
        OnDragEnd.Invoke();
    }
}
