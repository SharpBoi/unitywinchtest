﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util {
    public static T getOrCreate<T> (GameObject go) where T : Component {
        Component c = go.GetComponent<T>();
        if (c) return c as T;
        else return go.AddComponent<T>();
    }

}
