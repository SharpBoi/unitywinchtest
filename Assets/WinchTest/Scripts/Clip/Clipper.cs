﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public enum ClipperRole { Host, Child }

public class Clipper : MonoBehaviour {

    public static List<Action<Clipper>> ClipRequests = new List<Action<Clipper>>();

    [Header("Clip settings")]
    public int Index = 0;
    public ClipperRole Role = ClipperRole.Child;
    public bool CanRequestClip;
    public Vector3 ClipOffset;

    [Header("Events")]
    public UnityEvent OnClip;
    public UnityEvent OnUnclip;


    private Clipper otherClipper;
    private Collider otherClipperCollider;
    private Coroutine clipUpdate;
    private ConfigurableJoint joint;
    private Rigidbody rigid;


    void Start () {
        ClipRequests.Add(onClipRequest);

        rigid = GetComponent<Rigidbody>();
    }
    private void OnDestroy () {
        ClipRequests.Remove(onClipRequest);
    }

    private void onClipRequest (Clipper clipable) {
        if (clipable == otherClipper) {
            Clip();
            otherClipper.Clip();
        }
    }

    public void Clip () {
        otherClipper.otherClipper = this;

        if (Role == ClipperRole.Child) {
            joint = Util.getOrCreate<ConfigurableJoint>(gameObject);
            joint.axis = new Vector3(1, 0, 0);
            joint.secondaryAxis = new Vector3(0, 1, 0);
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;
            joint.projectionMode = JointProjectionMode.PositionAndRotation;
        }
        if (Role == ClipperRole.Host) {
            otherClipper.joint.anchor = otherClipper.ClipOffset;
            //otherClipper.transform.position = this.transform.TransformPoint(this.ClipOffset);
            this.transform.position = otherClipper.transform.TransformPoint(otherClipper.ClipOffset);
            otherClipper.joint.connectedBody = this.rigid;
        }

        OnClip.Invoke();
    }
    public void Unclip () {
        if (otherClipper) {
            if (Role == ClipperRole.Child) {
                joint.connectedBody = null;
                Destroy(joint);
            }

            Clipper c = otherClipper;
            otherClipper = null;
            otherClipperCollider = null;
            c.Unclip();

            OnUnclip.Invoke();
        }

        Debug.Log(otherClipper);
    }

    public void TryClipClosest () {
        if (CanRequestClip) {
            fireAllRequests();
        }
    }
    public void TryUnclipAny () {
        Unclip();
    }

    private void fireAllRequests () {
        ClipRequests.ForEach((a) => {
            a(this);
        });
    }

    private void OnTriggerEnter (Collider other) {
        Clipper clipper = other.GetComponent<Clipper>();
        if (clipper) {
            for (int i = 0; i < SOContainer.main.ClipTable.Indices.Length; i++) {
                if (SOContainer.main.ClipTable.Indices[i].ClipperIndex == Index) {
                    if (ArrayUtility.Contains(SOContainer.main.ClipTable.Indices[i].ClipableIndices, clipper.Index)) {
                        otherClipperCollider = other;
                        otherClipper = clipper;
                    }
                }
            }
        }
    }
    private void OnTriggerExit (Collider other) {
        if (other == otherClipperCollider) {
            otherClipper = null;
            otherClipperCollider = null;
        }
    }

    private void OnDrawGizmos () {
        Gizmos.DrawSphere(transform.TransformPoint(ClipOffset), 0.05f);
    }
}
