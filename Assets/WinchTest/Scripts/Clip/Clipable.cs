﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Clipable : MonoBehaviour {

//    #region FIELDS
//    public static List<Action<Clipable>> ClipRequests = new List<Action<Clipable>>();


//    public int Index = 0;


//    private Clipper crntClipper;
//    #endregion

//    #region PROPS
//    public bool IsClipped { get { return crntClipper != null; } }
//    #endregion

//    public bool Clip (Clipper clipper) {
//        if (crntClipper) return false;
//        else {
//            crntClipper = clipper;
//            Debug.Log("clip clipable");
//            return true;
//        }
//    }
//    public void Unclip () {
//        if (crntClipper) {
//            Debug.Log("UNclip clipable");
//            Clipper c = crntClipper;
//            crntClipper = null;
//            c.Unclip();
//        }
//    }

//    public void TryClipClosest () {
//        fireAllRequests();
//    }
//    public void TryUnclipAny () {
//        Unclip();
//    }

//    private void fireAllRequests () {
//        ClipRequests.ForEach((a) => {
//            a(this);
//        });
//    }
//}
