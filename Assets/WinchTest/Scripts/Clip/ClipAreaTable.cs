﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "ClipAreaTableAsset", menuName = "ClipAreaTable", order = 1)]
public class ClipAreaTable : ScriptableObject {

    [SerializeField]
    public ClipperIndeces[] Indices;

    private void Awake () {
    }
}

[System.Serializable]
public struct ClipperIndeces {
    public int ClipperIndex;
    public int[] ClipableIndices;
}
