﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    #region FIELDS
    public Vector3 Axis = new Vector3(1, 0, 0);
    public float Speed = 0.01f;


    private bool isRotateNow = false;
    private float angle = 0;
    #endregion

    #region PROPS
    public bool Inverse { get; set; }
    #endregion

    void Start () {

    }


    private void FixedUpdate () {
        if (isRotateNow) {
            angle += Speed * (Inverse ? -1 : 1);
            transform.localRotation = Quaternion.AngleAxis(angle, Axis);
        }
    }

    public void StartRotate () {
        isRotateNow = true;
    }
    public void StopRotate () {
        isRotateNow = false;
    }
}
